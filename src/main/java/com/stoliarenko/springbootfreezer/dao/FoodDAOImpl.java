package com.stoliarenko.springbootfreezer.dao;

import com.stoliarenko.springbootfreezer.entity.Food;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class FoodDAOImpl implements FoodDAO {

	//define field for entitymanager
	private EntityManager entityManager;

	//set up construceot injection
	@Autowired
	public FoodDAOImpl(EntityManager theEntityManager){
		entityManager = theEntityManager;
	}
			
	@Override
	public List<Food> getFoods() {


		// get the current hibernate session

		Session currentSession = entityManager.unwrap(Session.class);
				
		// create a query  ... sort by last name
		Query<Food> theQuery =
				currentSession.createQuery("from Food order by name",
											Food.class);
		
		// execute query and get result list
		List<Food> foods = theQuery.getResultList();
				
		// return the results		
		return foods;
	}

	@Override
	public List<Food> getFoodByName(String foodName) {

		// get the current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);

//		// create a query  ... sort by last name
		Query<Food> theQuery =
				currentSession.createQuery("from Food where name=:foodName order by name",
						Food.class);

		theQuery.setParameter("foodName", foodName);

		// execute query and get result list
		List<Food> foods = theQuery.getResultList();

		// return the results
		return foods;

	}

	@Override
	public void saveFood(Food theFood) {

		// get current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);
		
		// save/upate the customer ... finally LOL
		currentSession.saveOrUpdate(theFood);
		
	}

	@Override
	public Food getFood(int theId) {

		// get the current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);
		
		// now retrieve/read from database using the primary key
		Food theFood = currentSession.get(Food.class, theId);
		
		return theFood;
	}

	@Override
	public void deleteFood(int theId) {

		// get the current hibernate session
		Session currentSession = entityManager.unwrap(Session.class);
		
		// delete object with primary key
		Query theQuery =
				currentSession.createQuery("delete from Food where id=:foodId");
		theQuery.setParameter("foodId", theId);
		
		theQuery.executeUpdate();		
	}



}











