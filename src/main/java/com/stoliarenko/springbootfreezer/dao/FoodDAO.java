package com.stoliarenko.springbootfreezer.dao;



import com.stoliarenko.springbootfreezer.entity.Food;

import java.util.List;

public interface FoodDAO {

	public List<Food> getFoods();

	public void saveFood(Food theFood);

	public Food getFood(int theId);

	public void deleteFood(int theId);

	public List<Food> getFoodByName(String foodName);
	
}
