package com.stoliarenko.springbootfreezer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootFreezerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootFreezerApplication.class, args);
	}

}
