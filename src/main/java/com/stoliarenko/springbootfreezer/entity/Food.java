package com.stoliarenko.springbootfreezer.entity;

import javax.persistence.*;

@Entity
@Table(name="food")
public class Food {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="id")
	private int id;

	@Column(name="name")
	private String name;
	
	@Column(name="type")
	private String foodType;
	
	@Column(name="description")
	private String description;
	
	@Column(name="quantity")
	private int quantity;
	
	public Food() {
		
	}

	public Food(String name, String foodType, String description, int quantity) {
		this.name = name;
		this.foodType = foodType;
		this.description = description;
		this.quantity = quantity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFoodType() {
		return foodType;
	}

	public void setFoodType(String foodType) {
		this.foodType = foodType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + foodType + ", lastName=" + description + ", email=" + quantity + "]";
	}
		
}





